

using UnrealBuildTool;
using System.Collections.Generic;

public class OnlineTarget : TargetRules
{
	public OnlineTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Online" } );
	}
}
